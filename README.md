# browse-with-threads

Le problème posé consiste à écrire un algorithme efficace déterminant si une valeur d’un tableau
non trié y est présente au moins une deuxième fois ; le résultat est donc un simple booléen. Cette
recherche est normalement de complexité linéaire en la